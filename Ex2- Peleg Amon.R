install.packages('tm')
library('tm')
#text mining package
install.packages('e1071')
library('e1071')


trainTitanic<-read.csv('train.csv', stringsAsFactors = FALSE)
testTitanic<-read.csv('test.csv', stringsAsFactors = FALSE)


install.packages('mice')
library('mice')
# this packege  dealing with missing data
dim(trainTitanic)
#we have 12 columns

str(trainTitanic)
#deleting e-relavent columns
trainTitanic <- trainTitanic[,c(2,3,5,6)]

testTitanic <- testTitanic[,c(2,4,5)]

str(trainTitanic)
str(testTitanic)
summary(trainTitanic)

#devide the age column to old, mature and child = OMC
convert_OMC <-function(x){
  x <-ifelse(x>17,ifelse(x>50,2,1),0)
  x <- factor(x,level=c(2,1,0), labels =c('old','mature','child'))
}
#change my columns to factors
trainTitanic$Survived <- as.factor(trainTitanic$Survived)
#apply the function on the age element
trainTitanic[4:4] <- apply(trainTitanic[4:4],MARGIN=1:2,convert_OMC)
trainTitanic[4:4]
testTitanic[3:3] <- apply(testTitanic[3:3],MARGIN=1:2,convert_OMC)
testTitanic[3:3]

trainTitanic$Pclass <- as.factor(trainTitanic$Pclass)
testTitanic$Pclass <- as.factor(testTitanic$Pclass)

trainTitanic$Age <- as.factor(trainTitanic$Age)
testTitanic$Age <- as.factor(testTitanic$Age)

trainTitanic$Sex <- as.factor(trainTitanic$Sex)
testTitanic$Sex <- as.factor(testTitanic$Sex)


str(trainTitanic)
str(testTitanic)

df_train <- as.data.frame(trainTitanic)
df_train
df_test <- as.data.frame(testTitanic)
df_test
#create a column 'survived' on the test set
df_test["survived"] <- ''

#remove NA
df_train <- na.omit(df_train) 
df_test <- na.omit(df_test) 
#remove column 4
model <- naiveBayes(df_train[,2:4],df_train[,1]) 
model

install.packages('SDMTools')
library(SDMTools)
#prediction on the test  - survived?
prediction <- predict(model,df_test[-4])
prediction



confusion.matrix(df_train$Survived,prediction)

#////////////////// confusion matrix only on training set

split <- runif(nrow(trainTitanic)) >0.3
split
TitanicTrain1 <- trainTitanic[split,]
TitanicTest1 <- trainTitanic[!split,]

#data frame
df_train1 = as.data.frame(TitanicTrain1)
df_test1 = as.data.frame(TitanicTest1)
#remove NA
df_train1 <- na.omit(df_train1) 
df_test1 <- na.omit(df_test1) 
#remove column 1
model1 <- naiveBayes(df_train1[,2:4],df_train1[,1]) 
model1

prediction1 <- predict(model1,df_test1[-1])
prediction1

conv_10 <-function(x){
  x <-ifelse(x==0,0,1)
}

pred01 <- sapply(prediction1,conv_10)
actual01 <- sapply(df_test1$Survived,conv_10)
confusion.matrix(actual01,pred01)

prediction2 <- predict(model1,df_train1[-1])
prediction2

conv_10 <-function(x){
  x <-ifelse(x==0,0,1)
}

pred02 <- sapply(prediction2,conv_10)
actual02 <- sapply(df_train1$Survived,conv_10)
confusion.matrix(actual02,pred02)

